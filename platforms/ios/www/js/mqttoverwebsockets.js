
var connected = false;

$(function() {
	if ($('#url').val() == '')
		$('#url').val(
				window.location.protocol + '//' + window.location.hostname
						+ ':8443/api');
});

var portraitScreenHeight;
var landscapeScreenHeight;
var showModalRefresh = function(){
	$("#modal").html("Scanning for tags...");
	$("#modalPage").show();
}
var hideModal = function(){
	$("#modalPage").hide();
}

var createThing = function(){
	var thingkey = "iPhone-"+device.uuid;
	var lastFive = thingkey.substr(thingkey.length - 2); 
	var name = "iPhone- "+lastFive;
	if (connected == false) {
		alert('Connect first!');
		return;
	}
	var req = 
	'{'+
		'"createThing": {'+
			'"command": "thing.create",'+
			'"params": {'+
				'"name": "'+name+'",'+
				'"key": "'+thingkey+'",'+      
				'"attrs": {'+  
					'"name": {'+  
					  '"value": "Name not set"'+  
					'}'+  
				  '},'+  
				'"defKey": "iphone",' +
				'"desc": "",'+
				'"tags": ["iPhone"],'+
				'"iccid": "",'+
				'"esn": ""'+
			'}'+
		'}'+
	'}';

	try {
		JSON.parse(req);
	} catch (e) {
		alert('Invalid JSON!' + e)
		return;
	}

	message = new Paho.MQTT.Message(req);
	
	message.destinationName = "api";
	client.send(message);
}

var publishattr = function (verPub, typePub, fwPub, uuidPub) {

	if (connected == false) {
		alert('Connect first!');
		return;
	}

	var thingkey = "iPhone-" + device.uuid;
	var req =
	'{' +
		'"publishAttr": {' +
			'"command": "attribute.batch",' +
			'"params": {' +
				'"thingKey": "' + thingkey + '",' +
				'"key": "batchattrkey",' +
				'"data": [';

	req += '{' +
		'"key": "Type",' +
		'"value": "' + typePub + '"' +
	'},';

	req += '{' +
		'"key": "FW",' +
		'"value": "' + fwPub + '"' +
	'},';

	req += '{' +
		'"key": "UUID",' +
		'"value": "' + uuidPub + '"' +
	'},';

	req += '{' +
		'"key": "Ver",' +
		'"value": "' + verPub + '"' +
	'},';


	req = req.substring(0, req.length - 1);
	req += ']}}}';
	console.log(req);

	try {
		JSON.parse(req);
	} catch (e) {
		alert('Invalid JSON!' + e)
		return;
	}

	message = new Paho.MQTT.Message(req);

	message.destinationName = "api";
	client.send(message);
}

var publishVals = function (battPub, chargePub, accxPub, accyPub, acczPub, latPub, lonPub, gpsAccPub, headPub) {
	var isCharging;

	if (connected == false) {
		alert('Connect first!');
		return;
	}

	if (chargePub)
		isCharging = 1;
	else
		isCharging = 0;

	var thingkey = "iPhone-" + device.uuid;
	var req = 
	'{'+
		'"publishVals": {' +
			'"command": "property.batch",'+
			'"params": {'+
				'"thingKey": "'+thingkey+'",'+
				'"key": "batchkey",'+
				'"aggregate":false,'+
				'"data": [';

		req +=	'{'+
			'"key": "battLevel",'+
			'"value": ' + battPub +
		'},';

		req +=	'{'+
			'"key": "isCharge",'+
			'"value": ' + isCharging +
		'},';

		req +=	'{'+
			'"key": "accX",'+
			'"value": ' + accxPub +
		'},';

		req += '{' +
			'"key": "accY",' +
			'"value": ' + accyPub +
		'},';

		req += '{' +
			'"key": "accZ",' +
			'"value": ' + acczPub +
		'},';

		req += '{' +
			'"key": "lat",' +
			'"value": ' + latPub +
		'},';

		req += '{' +
			'"key": "lon",' +
			'"value": ' + lonPub +
		'},';

		req += '{' +
			'"key": "gpsAcc",' +
			'"value": ' + gpsAccPub +
		'},';

		req += '{' +
			'"key": "heading",' +
			'"value": ' + headPub +
		'},';

	req = req.substring(0, req.length - 1);
	req +=	']}}}';	
	console.log(req);

	try {
		JSON.parse(req);
	} catch (e) {
		alert('Invalid JSON!' + e)
		return;
	}	

	message = new Paho.MQTT.Message(req);
		
	message.destinationName = "api";
	client.send(message);
}

var gotResults = function(msg) {
	try {
		msg = JSON.stringify(JSON.parse(msg), undefined, 4);
	} catch (e) {
		alert('Invalid JSON!' + e)
	}
}

var subscribe = function() {

	if (connected == false) {
		alert('Connect first!');
		return;
	}

	var topic = "thing/#iPhone";

	client.subscribe(topic);

	//$('#subscribe-label').html('Topic: <b>' + topic + '</b>');

	//$('#subscribe-box').html('');
}


var connectPortal = function (host, password) {

	var port = 443;

	var ssl = port != 80 && port != 8080
	
	var thingkey = "iPhone-"+device.uuid;
	console.log("connectPortal" + host + " " + password);
	console.log(thingkey);
	
	var clientId = thingkey;
	var username = thingkey;
	
	if(connected==true){
		return;
	}
	
	console.log("connectPortal: " + host + " " + port + " " + password);
	client = new Paho.MQTT.Client(host, port, '/mqtt' + (ssl ? '-ssl' : ''),
			clientId);

	client.onConnectionLost = onConnectionLost;
	client.onMessageArrived = onMessageArrived;
	
	client.connect({
		userName : username,
		password : password,
		useSSL : ssl,
		mqttVersion : 3,
		onSuccess : onConnect,
		onFailure : onFailure
	});

	function onFailure(responseObject) {
		console.log("failed");
		alert('Failed to connect: ' + responseObject.errorMessage);
	}

	function onConnect() {
		connected=true;
		console.log("connected");
//		createThing();
//		getName();
    
		function disp(pos) {
			lat = pos.coords.latitude;
			lon = pos.coords.longitude;
		}
//		navigator.geolocation.getCurrentPosition(disp);
		
		client.subscribe("reply");
	}

	function onConnectionLost(responseObject) {

	}

	function onMessageArrived(message) {
		var obj = jQuery.parseJSON(message.payloadString);
		$("#modalPage").hide();
		if(obj['getName']){			
			if(obj['getName'].errorCodes){
				 return;
			}
			var name = obj['getName'].params.value;
			$("#name").val(name);
		}
	}
}
var littleEndianToUint8ZeroethIndex = function(data)
{
	return data[0];
}
var littleEndianToUint8FirstIndex = function(data)
{
	return data[1];
}
var littleEndianToUint16 = function(data)
{
	return (littleEndianToUint8FirstIndex(data) << 8) +
		littleEndianToUint8ZeroethIndex(data);
}
