var isCloudConnected = false;
var version = "0.0.1";
var server = "open-api.devicewise.com";
var token = "hoJQ1OLkQSWBBY2K"; //OPEN-IPHONE";
var scheduledInt;
var battPub = 0, chargePub = 0, accxPub = 0, accyPub = 0, acczPub = 0, latPub = 0, lonPub = 0, gpsAccPub = 0, headPub = 0;
var storage = window.localStorage;
var value;

function onProximityState(state) {
	console.log("onProximityState");

	console.log('Proximity state: ' + (state ? 'near' : 'far'));
};

var app = {
	initialize: function () {
		console.log("initialize1");

		this.bindEvents();

	},
	bindEvents: function () {
		document.addEventListener('deviceready', this.onDeviceReady, false);
		connectionBtn.addEventListener('touchstart', this.onCloudConnection, false);
		//		disconnectButton.addEventListener('touchstart', this.disconnect, false);
	},
	onDeviceReady: function () {
		if (storage.length == 0) {
			console.log("Empty storage");
			storage.setItem("sServer", server);
			storage.setItem("sToken", token);
		}
		else {
			console.log("Full storage, length:" + storage.length);
			server = storage.getItem("sServer");
			token = storage.getItem("sToken");
		};
		document.getElementById("nameSer").value = server;
		document.getElementById("nameTok").value = token;

		// Get Bettery parameters
		window.addEventListener("batterystatus", onBatteryStatus, false);

		window.plugins.insomnia.keepAwake();
		// Get device parameters
		document.getElementById("modType").innerHTML = device.model;
		document.getElementById("modImei").innerHTML = device.uuid;
		document.getElementById("modFw").innerHTML = device.version;
		
		// Get Accelerometer parameters
		var optionsAccel = { frequency: 1000 };  // Update every 1 second
		var watchAccel = navigator.accelerometer.watchAcceleration(onAccelSuccess, app.onError, optionsAccel);

		// Get GPS parameters
		var watchGps = navigator.geolocation.watchPosition(onGps, onGpsError, { enableHighAccuracy: true });

		// Get Compass parameters
		var optionsCompass = {	frequency: 1000	}; // Update every 1 second
		var watchCompass = navigator.compass.watchHeading(onCompassSuccess, onCompassError, optionsCompass);

	},
	
	onCloudConnection: function () {
		if (isCloudConnected) {
			isCloudConnected = false;
			console.log("Disconnect");
			document.getElementById("connectionBtn").innerHTML = "CONNECT";
			clearInterval(scheduledInt);
		}
		else {
			isCloudConnected = true;
			document.getElementById("connectionBtn").innerHTML = "DISCONNECT";
			console.log("Connect11" + server + " " + token);
			server = document.getElementById("nameSer").value;
			token = document.getElementById("nameTok").value;
			storage.removeItem("sServer");
			storage.removeItem("sToken");
			storage.setItem("sServer", server);
			storage.setItem("sToken", token);
			console.log("Connect22" + server + " " + token);
			connectPortal(server, token);
			setTimeout(function () {
				publishattr(version, device.model, device.version, device.uuid);
				scheduledInt = setInterval(function () {
					if (isCloudConnected)
						publishVals(battPub, chargePub, accxPub, accyPub, acczPub, latPub, lonPub, gpsAccPub, headPub);
				}, 5000);
			}, 5000);
		}
	},
	
	/*	disconnect: function (event) {
			var deviceId = event.target.dataset.deviceId;
			ble.disconnect(deviceId, app.showMainPage, app.onError);
			console.log("write disconnect2 Error");
		},*/
	showMainPage: function (devId) {
		//		listItem.dataset.deviceId == devId ?
		connectedDeviceID = 0;
		$("#mainPage").show();
	},

	onError: function (reason) {
		searchingForDevices = false;
		console.log("onError: " + reason);
	}
};

function onBatteryStatus(status) {
	battPub = status.level;
	chargePub = status.isPlugged;
	console.log("Level: " + battPub + " isPlugged: " + chargePub);
	document.getElementById("modBattPer").innerHTML = battPub + "%";
	document.getElementById("modBattCharge").innerHTML = chargePub ? "YES" : "NO";
}

function onAccelSuccess(acceleration) {
	accxPub = acceleration.x.toFixed(8);
	accyPub = acceleration.y.toFixed(8);
	acczPub = acceleration.z.toFixed(8);
	document.getElementById("modAccelX").innerHTML = accxPub;
	document.getElementById("modAccelY").innerHTML = accyPub;
	document.getElementById("modAccelZ").innerHTML = acczPub;
}

function onGps(pos) {
	latPub = pos.coords.latitude;
	lonPub = pos.coords.longitude;
	gpsAccPub = pos.coords.accuracy;
	console.log("lat: " + latPub + " lon: " + lonPub);
	document.getElementById("modLatitude").innerHTML = latPub.toFixed(8);
	document.getElementById("modLongitude").innerHTML = lonPub.toFixed(8);
	document.getElementById("modAccuracy").innerHTML = gpsAccPub;
}

function onGpsError(err) {
	console.log("Err code: " + err.code + " Error message: " + err.message);

}

function onCompassSuccess(heading) {
	headPub = heading.magneticHeading.toFixed(8);
	document.getElementById("modHeading").innerHTML = headPub;
};

function onCompassError(compassError) {
	alert('Compass error: ' + compassError.code);
};

